const Client = require('./client');

module.exports = ({host, port, endpoint, user, password}) => {
	if (!host) {
		throw new Error('"host" is missing');
	}
	if (!port) {
		throw new Error('"port" is missing');
	}
	if (!endpoint) {
		throw new Error('"endpoint" is missing');
	}

	return new Client(host, port, endpoint, user, password);
};